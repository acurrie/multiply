 <?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
    $plugin_info = array(
        'pi_name'           => 'Multiply',
        'pi_version'        => '1.0',
        'pi_author'         => 'Andrew Currie',
        'pi_author_url'     => 'https://digitalpci.com',
        'pi_description'    => 'Multiply numbers or fields delimited by |',
        'pi_usage'          => Multiply::usage()
        );
     
    class Multiply {
     
        public $return_data = "";
     
        public function __construct() {
            $this->EE =& get_instance();
            $nums = $this->EE->TMPL->fetch_param('numbers');
     
            $nums = explode('|', $nums);
            $product = 1;
     
            foreach ($nums as $num) {
                    $product = $product * $num;
            }
     
     
            $this->return_data = $product;
        }
     
        public static function usage() {
     
            ob_start();  ?>
     
            Example: {exp:multiply numbers="5|2"} 
            Outputs: 10

            You can also use EE tags similiar to the example below:
            {exp:multiply numbers="{field_1}|{field_2}"}
            
            Outputs: The result of multiplying the numeric values in {field_1} and {field_2}
     
            <?php
            $buffer = ob_get_contents();
            ob_end_clean();
     
            return $buffer;
        }
    }